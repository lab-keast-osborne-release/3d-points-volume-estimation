# 3D-points-volume-estimation

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

`3D Points Volume Estimation` is a Python workflow for estimating the 3D volume of a cloud of points. 

It was used to visualize and quantify the dendritic field volume of preganglionic neurons in the rat lumbosacral spinal cord. These were identified as punctate fluorescence in neurons labelled with the retrograde neural tracer CTB (cholera toxin subunit B). This was detected in 3D multichannel images of whole mount cleared rat spinal cord acquired by lightsheet fluorescence microscopy (LSFM, LaVision Ultramicroscope).

This workflow is cited in a [bioRxiv preprint by Fuller-Jackson et al. (2024)](https://www.biorxiv.org/)


![Alt text](media_for_readme/img.png "Sample image") _Sample 3D Multichannel Image of a rat lumbosacral spinal cord acquired by LSFM. The gray channel is the raw image shown as a maximum intensity projection of CTB-immunofluorescence in retrogradely labelled  preganglionic neurons. The orange channel shows CTB puncta detected with point detection (Imaris). The workflow was used to fit the volume occupied by the dendritic field (magenta).This dendritic field volume defines the perisomatic volume occupied by the neurons and their dendrites._

![Video Demonstration](media_for_readme/r22-731-l6s1-convex-hull-1_3.mp4)

# Python Code

The python code **distributed_points_volume_estimation.py** implements the steps of estimating the 3D volume of a cloud of points:
1. reading inputs (3D point coordinates should be listed in .XYZ file. See the sample input file in `\test_data` folder.)
2. filtering outlier points
3. generating alpha shape (for selecting points to generate small convex hulls)
4. volume computing (unioning small convex hulls and computing the volume of the watertight shape)
5. ploting (by plotly)

Output:
- Results of each step will be output in the console.
- The unioned convex hull 3D models (in Step 4): unioned_convex_hull.obj and .wrl will be exported to the given input path.
- Plotly webpage (.html) presentation of input points, alpha shape, and unioned convex hull.

![Alt text](media_for_readme/html_plot.png "Plot result")


## run with sample data
The code can execute the sample data in `test_data` folder with pre-set peremeters by:

`$ python distributed_points_volume_estimation.py`

in the console. Python libraries required to run the code are:
- [ ] numpy
- [ ] pandas
- [ ] trimesh
- [ ] pymeshlab
- [ ] plotly

## use the code for your own data
If you want to use this Python code to compute the volume of 3D cloud points in your own data,
you need to set the parameters:

`path` : directory to the .xyz file.

`points_file_name` : name of the .xyz file recording point coordinates.

`filter_alpha` : alpha of alpha shape for filtering outliers;
                 more points are filtered out with smaller alpha;
                 usually 0.4.

`alpha` : alpha of alpha shape for volume computation.

`START, STEP, BACK` : refer to Section 4 for method description commented in the code;
                      values are set based on the point number and distribution.

either in the main function, or when calling the function: **esitmate_distributed_points_volume_by_alpha_shape**(path, points_file_name,
                                                      filter_alpha,
                                                      alpha,
                                                      START,
                                                      STEP,
                                                      BACK)
