
#!/usr/bin/env python

"""
This script is the implementation of the method for estimating volume of 3D cloud points, published in 
`A 3D atlas of sexually dimorphic lumbosacral motor neurons that control and integrate pelvic visceral and somatic functions in rats`
by John-Paul Fuller-Jackson, Ziying Yang, Nicole Wiedmann, Alan Watson, Nathaniel Jenkins, Janet Keast, and Peregrine Osborne,
The Journal of Neuroscience.

Script by Ziying Yang.
2024 MAR.
"""

import numpy as np
import pandas as pd

import trimesh
import pymeshlab

import plotly.graph_objects as go
from plotly.offline import download_plotlyjs, iplot
from plotly.graph_objs import *

import warnings
warnings.filterwarnings('ignore')


def esitmate_distributed_points_volume_by_alpha_shape(path, points_file_name,
                                                      filter_alpha,
                                                      alpha,
                                                      START,
                                                      STEP,
                                                      BACK):

    """
        Main function of computing the volume of adjusted convex hull
        of points filtered and selected by alpha shape.

        
        Parameters
        ----------
        path : directory to the .xyz file.
        points_file_name : name of the .xyz file recording point coordinates. see example in ./test_data/.
        filter_alpha : alpha of alpha shape for filtering outliers;
                       more points are filtered out with smaller alpha;
                       usually 0.4.
        alpha : alpha of alpha shape for volume computation.
        START, STEP, BACK : refer to Section 4 for method description;
                            values are set based on the point number and distribution.


        Console Output (using example in ./test_data/)
        --------------

        ========== 1. Reading inputs ==========

        >>> 85406 points are loaded.


        ===== 2. Filtering outlier points =====

        >>> With alpha = 0.4 for filtering outliers,
            2855 outlier points are removed.
            82551 points remain for generating alpha shape.


        ====== 3. Generating alpha shape ======

        >>> With alpha = 4,
            1388 points are on the generated alpha shape surface.


        ========= 4. Computing volume =========

        >>> START = 1340, STEP = 100, BACK = 30
            Unioned convex hull volume: 933314263.8658233

        >>> WRL and OBJ files have been exported to: ./test_data/

        ============= 5. Ploting ==============

        Done.


        File Output
        -----------
        The unioned convex hull 3D models:
        unioned_convex_hull.obj and .wrl
        will be exported to 'path'.

        Plotly webpage (.html) presentation of
        input points, alpha shape, and unioned convex hull.



    """

    mesh_list = []

    #============ 1. Loading Input ============#
    print("\n========== 1. Reading inputs ==========")

    # read point coordinates as input
    ms = pymeshlab.MeshSet()
    ms.load_new_mesh(path + points_file_name)
    m = ms.current_mesh()
    total_points_len = len(ms.current_mesh().vertex_matrix())

    # plot points
    px, py, pz = m.vertex_matrix()[:,:3].T
    points = go.Scatter3d(mode = 'markers',
                        name = 'original points',
                        x =px,
                        y= py,
                        z= pz,
                        marker = Marker(size=2, color='#458B00' )
            )

    mesh_list.append(points)

    print("\n>>> " + str(total_points_len) + " points are loaded.")


    #========== 2. Filtering Outliers ==========#

    print("\n\n===== 2. Filtering outlier points =====")

    # filter out outliers using alpha shape
    # 
    ms.generate_alpha_shape(alpha = pymeshlab.Percentage(filter_alpha))

    # remove points outside alpha shape (for filtering)
    ms.meshing_remove_unreferenced_vertices()

    print("\n>>> With alpha = "+str(filter_alpha)+" for filtering outliers,")
    print("\t"+str(total_points_len-len(ms.current_mesh().vertex_matrix()))+" outlier points are removed." )
    print("\t"+str(len(ms.current_mesh().vertex_matrix()))+" points remain for generating alpha shape.")

    selected_dots=ms.current_mesh().vertex_matrix()


    #========== 3. Generating Alpha Shape ==========#

    print("\n\n====== 3. Generating alpha shape ======")

    ms.generate_alpha_shape(alpha = pymeshlab.Percentage(alpha), filtering='Alpha Shape')
    ms.meshing_remove_unreferenced_vertices()

    m = ms.current_mesh()
    vertices = m.vertex_matrix()
    faces=m.face_matrix()

    print("\n>>> With alpha = " + str(alpha) + ",")
    print("\t" + str(len(vertices)) + " points are on the generated alpha shape surface.")

    # plot alpha shape
    x, y, z = vertices[:,:3].T
    if len(faces)>0:
        I, J, K = faces.T

        mesh = go.Mesh3d(x=x,
                         y=y,
                         z=z,
                         i=I,
                         j=J,
                         k=K,
                         name="alpha shape", 
                         showlegend=True,
                         showscale=False,
                         opacity=0.7,
                         color="#eedd78")
        mesh_list.append(mesh)


    #========== 4. Computing Volume ==========#
    # To compute the volume of the 3D space wrapped by alpha shape in step 3,
    # a watertight polygon mesh covering the same 3D space need to be estimated by:
    #       4.1. sort the vertices of alpha shape along Z axis;
    #       4.2. from START-th, select the number of STEP vertices, and generate convex hull;
    #       4.3. set START = START + STEP - BACK, repeat 4.2 till the last vertice is included;
    #       4.4. union all small convex hulls computed in 4.2 and 4.3;
    #       4.5. remove the wall of the conjunction parts inside the unioned convex hull;
    #       4.6. compute the volume of the unioned convex hull.

    print("\n\n========= 4. Computing volume =========")

    alpha_trimesh = trimesh.Trimesh(vertices=vertices, faces=faces, process=False)
    vertices=vertices[vertices[:, 1].argsort()] #4.1

    master_convex_hull = pymeshlab.MeshSet()
    while(START + STEP < len(vertices)): #4.2
        hull_trimesh = trimesh.Trimesh(vertices=vertices[START : START + STEP], process=False)
        ms_hull = hull_trimesh.convex_hull
        master_convex_hull.add_mesh(pymeshlab.Mesh(ms_hull.vertices, ms_hull.faces))

        START = START + STEP - BACK #4.3

    # last small convex hull
    hull_trimesh = trimesh.Trimesh(vertices=vertices[START:], process=False)
    ms_hull=hull_trimesh.convex_hull
    master_convex_hull.add_mesh(pymeshlab.Mesh(ms_hull.vertices, ms_hull.faces))


    # 4.5
    unid=0
    for i in range(1,len(master_convex_hull),1):
        master_convex_hull.generate_boolean_union(first_mesh=unid, second_mesh=i)
        unid = master_convex_hull.current_mesh_id()

    # 4.6
    out_dict = master_convex_hull.get_geometric_measures()
    print("\n>>> START = " + str(START) + ", STEP = " + str(STEP) + ", BACK = " + str(BACK))
    print("\tUnioned convex hull volume: " + str(out_dict['mesh_volume']))

    # plot convex hull
    vertices = master_convex_hull.current_mesh().vertex_matrix()#alpha_trimesh.vertices
    faces = master_convex_hull.current_mesh().face_matrix()#alpha_trimesh.faces
    x, y, z = vertices[:,:3].T
    if len(faces)>0:
        I, J, K = faces.T
        mesh = go.Mesh3d(x=x,
                         y=y,
                         z=z,
                         i=I,
                         j=J,
                         k=K,
                         name="unioned convex hull", 
                         showlegend=True,
                         showscale=False,
                         opacity=0.5,
                         color="#00ee00")
        mesh_list.append(mesh)


    #save convex hull to .wrl and .obj
    color_v = master_convex_hull.current_mesh().vertex_color_matrix()
    color_v[:,0]=1   #r
    color_v[:,1]=0   #g
    color_v[:,2]=1   #b
    color_v[:,3]=0.2 #a

    color_f = np.zeros((faces.shape[0],4))
    color_f[:,0]=1   #r
    color_f[:,2]=1   #b
    color_f[:,3]=0.2 #a

    ms1 = pymeshlab.MeshSet()
    ms1.add_mesh(pymeshlab.Mesh(
            vertex_matrix=vertices,
            face_matrix=faces,
            v_color_matrix=color_v,
            f_color_matrix=color_f))

    ms1.save_current_mesh(path + "unioned_convex_hull.wrl")
    ms1.save_current_mesh(path + "unioned_convex_hull.obj")
    print("\n>>> WRL and OBJ files have been exported to: "+ path + "")


    #========== 5. Ploting and Saving 3D models ==========#

    print("\n\n============= 5. Ploting ==============")

    # plot layout
    layout = go.Layout(scene=dict(
                            xaxis=dict(visible=False),
                            yaxis=dict(visible=False),  
                            zaxis=dict(visible=False), 
                            aspectmode='data',  
                            camera=dict(eye=dict(x=1, y=-1, z=1)),
                            ),
                       legend=dict(font=dict(size=12,color="white")),
                       plot_bgcolor='rgb(20,20,20)',
                       paper_bgcolor='rgb(20,20,20)'
                      )

    fig = go.Figure(data=mesh_list, layout=layout)
    iplot(fig)
    #plotly.offline.plot(fig, filename='output.html')
    print("\nDone.\n")


if __name__ == "__main__":
    
    path = "./test_data/"
    points_file_name = "Points_XYZ.xyz"

    filter_alpha = 0.4  #alpha of alpha shape for filtering outliers
    alpha = 4           #alpha of alpha shape for volume computation

    START = 10
    STEP = 100         
    BACK = 30

    esitmate_distributed_points_volume_by_alpha_shape(path, points_file_name,
                                                      filter_alpha,alpha,
                                                      START,STEP,BACK)
